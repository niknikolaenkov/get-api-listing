<?php
/*
Plugin Name: Get API listing
Author: Nik Nikolaenkov
Author URI: NikNikolaenkov@gmail.com
*/

/*  Copyright 2017 Nik Nikolaenkov (email: NikNikolaenkov@gmail.com)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
require_once( ABSPATH . 'wp-admin/includes/file.php' );
require_once ABSPATH . 'wp-admin/includes/image.php';
require_once ABSPATH . 'wp-admin/includes/file.php';
require_once ABSPATH . 'wp-admin/includes/media.php';

wp_register_style( 'bootstrap-theme', plugins_url('/static/bootstrap-theme.css', __FILE__));
wp_register_style( 'bootstrap', plugins_url('/static/bootstrap.css', __FILE__));
wp_register_style( 'main_css', plugins_url('/static/main.css', __FILE__));
wp_enqueue_style( 'bootstrap-theme');
wp_enqueue_style( 'bootstrap');
wp_enqueue_style( 'main_css');
wp_register_script('minjs', plugins_url('/static/main.js', __FILE__));
wp_enqueue_script('minjs', plugins_url('/static/main.js', __FILE__));

function opinions_install(){
  global $wpdb;
  $table_name = $wpdb->prefix . "realtymx_title";
  $sql = "CREATE TABLE " . $table_name . " (
  	  id_title mediumint(9) NOT NULL AUTO_INCREMENT,
  	  title varchar(40) NOT NULL,
  	  UNIQUE KEY id (id_title)
  	)";
  require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
  dbDelta($sql);
  $table_name = $wpdb->prefix . 'realtymx_id';
  if($wpdb->get_var("SHOW TABLES LIKE $table_name") != $table_name) {
  $sql = "CREATE TABLE IF NOT EXISTS `$table_name` (
      `ID` BIGINT(20) UNSIGNED NOT NULL ,
      `DATA_LILT` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' ,
      `IMPORT_DATA` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' ,
      `IMPORT_ID` BIGINT(20) NOT NULL DEFAULT '0' ,
      `POSTID` BIGINT(20) NOT NULL DEFAULT '0' ,
      PRIMARY KEY (`ID`)
      ) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci";
      $wpdb->query($sql);}

  $url = 'http://dataapi.realtymx.com/listings/?apikey=472433504f385875&id=1021';
  $jsondata = file_get_contents($url);
  $json = json_decode($jsondata,true);
  $arr_kaylistind = [];
  foreach ($json['LISTINGS'] as $title_list => $data_list) {
      foreach ($data_list as $key => $value) {
  array_push($arr_kaylistind, $key);
      }
      break;
  }
  foreach ($arr_kaylistind as $key => $value) {
    $tablename = $wpdb->prefix . "realtymx_title";
    $data_list  =  $value;
    $sql = $wpdb->prepare("INSERT INTO `$tablename` (`title`) values (%s)", $data_list);
    $wpdb->query($sql);
  }
}

function opinions_uninstall(){
  global $wpdb;
	$table_name = $wpdb->prefix . 'realtymx_title';
	$sql = "DROP TABLE IF EXISTS $table_name";
	$wpdb->query($sql);
  $table_name = $wpdb->prefix . 'realtymx_id';
	$sql = "DROP TABLE IF EXISTS $table_name";
	$wpdb->query($sql);}

register_activation_hook(__FILE__, 'opinions_install'); //установить
register_deactivation_hook(__FILE__, 'opinions_uninstall');//удалить
add_action('admin_menu', 'listing_admin_menu');
add_action('wp_ajax_realtymx', 'start_ajax');

function listing_admin_menu(){
    add_submenu_page('realtymx_api', 'GetAPIlisting', 'Dashboard', 8, 'realtymx_api', 'control_set');
    add_submenu_page('realtymx_api', 'GetAPIlisting', 'Documentation', 8, 'realtymx_api&pg=documentation', 'control_set');
    add_submenu_page('realtymx_api', 'GetAPIlisting', 'Settings', 8, 'realtymx_api&pg=setting', 'control_set');
    add_menu_page('GetAPIlisting', 'RealtyMX API', 8, 'realtymx_api&pg=unloading&lis', 'control_set');

}
function control_set (){
    include_once("includes/index.php");

      if ($_GET['pg'] == "setting") include_once("includes/seting.php");

      if ($_GET['pg'] == "unloading") {
        // get_id_api();
        // unloading_data();
        include_once("includes/listin_board.php");
                                      }
      if ($_GET['pg'] == "documentation") include_once("includes/documentation.php");
}
function start_ajax(){
  include_once("includes/function.php");
      if ($_POST['api_post_meta'] == "new_id") {
        get_id_api();
        wp_die();
      }
      if ($_POST['type'] == "load") {
        $key_in = $_POST['load_post'];
        $key_in = array($key_in);
        unloading_data($key_in);
        wp_die();
      }
}
