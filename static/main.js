window.onload = function() {
    jQuery(function($) {
        var p_url = location.search.substring(1);
        var arr = p_url.split(/[&]/);

        if (arr[1] == "pg=unloading") {
            $('#1').removeClass("nav-tab");
            $('#1').addClass("nav-tab nav-tab-active");
        }
        if (arr[1] == "pg=documentation") {
            $('#2').removeClass("nav-tab");
            $('#2').addClass("nav-tab nav-tab-active");
        }
        if (arr[1] == "pg=setting") {
            $('#3').removeClass("nav-tab");
            $('#3').addClass("nav-tab nav-tab-active");
        }

        function funcBefore() {
            $("#resunt_info").text("data load...");

        }

        function funcSuccess() {
            $("#resunt_info").text(data);
        }

        $(document).ready(function() {
            $("#up_api").bind("click", function() {
                var new_id = "new_id";
                $.ajax({
                    url: "admin-ajax.php",
                    type: "POST",
                    data: "action=realtymx&api_post_meta=new_id",
                    beforeSend: funcBefore,
                    success: function(data) {
                        $('#resunt_info').html(data);
                    }
                });
            });
        });
        $('#load_post').click(function() {
            var vals = $(':checkbox').map(function(i, el) {
                if ($(el).prop('checked')) {
                    return $(el).val();
                }
            }).get();
            var str_id = (vals.join(","));
            if (str_id == '') {
                alert('Listings not selected');
            } else {
               var  arrID = str_id.split(',');
               (function updateInfo(i) {
                   if (i == arrID.length) {
                       return;
                   }
                   $.ajax({
                       type: "POST",
                       url: "admin-ajax.php",
                       data: "action=realtymx&" + "type=load" + "&load_post=" + arrID[i],
                       beforeSend: funcBefore,
                       success: function(data) {
                           $('#resunt_info').html(data);
                       }
                   }).always(function() {
                       updateInfo(i + 1);
                   });
               })(0);
            }
        });


        // var counter = 0;
        // function progressDemo() {
        // counter++;
        // document.getElementById( 'progress-bar' ).value = counter;
        // if( counter == 100 ) {
        // clearInterval( timer );
        // }
        // }
        // var timer = setInterval( progressDemo, 100 );
    });
}
