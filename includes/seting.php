<?php
require_once( ABSPATH . 'wp-admin/includes/file.php' );
require_once ABSPATH . 'wp-admin/includes/image.php';
require_once ABSPATH . 'wp-admin/includes/file.php';
require_once ABSPATH . 'wp-admin/includes/media.php';

echo "<h3> Property Features</h3>";
echo "<form action=\"admin.php?page=realtymx_api&pg=setting\"method=\"POST\">";
echo "<input type=\"hidden\" name=\"data\" value=\"out_set\">";
echo "<p class=\"submit\"><input type=\"submit\" name=\"api_post_meta\" id=\"property_mata\" class=\"button button-primary\" value=\"features\"></p>
</form>";
echo "<h3> Property Type</h3>";
echo "<form action=\"admin.php?page=realtymx_api&pg=setting\"method=\"POST\">";
echo "<input type=\"hidden\" name=\"data\" value=\"out_set\">";
echo "<p class=\"submit\"><input type=\"submit\" name=\"api_post_meta\" id=\"property_mata\" class=\"button button-primary\" value=\"type\"></p>
</form>";
echo "<h3> Property City</h3>";
echo "<form action=\"admin.php?page=realtymx_api&pg=setting\"method=\"POST\">";
echo "<input type=\"hidden\" name=\"data\" value=\"out_set\">";
echo "<p class=\"submit\"><input type=\"submit\" name=\"api_post_meta\" id=\"property_mata\" class=\"button button-primary\" value=\"city\"></p>
</form>";
echo "<h3> Property Status</h3>";
echo "<form action=\"admin.php?page=realtymx_api&pg=setting\"method=\"POST\">";
echo "<input type=\"hidden\" name=\"data\" value=\"out_set\">";
echo "<p class=\"submit\"><input type=\"submit\" name=\"api_post_meta\" id=\"property_mata\" class=\"button button-primary\" value=\"status\"></p>
</form>";
echo "-";
// echo "<pre>";
// print_r($_POST);
// echo "<pre>";
if (array_key_exists('api_post_meta', $_POST)) {
      if ($_POST[api_post_meta] == "features") {
        get_property_feature();
      }
      if ($_POST[api_post_meta] == "type") {
        get_property_type();
      }
      if ($_POST[api_post_meta] == "city") {
        get_property_city();
      }
      if ($_POST[api_post_meta] =="status") {
        get_property_status();
      }
}
?>
<?php
function get_property_feature(){
$url = 'http://dataapi.realtymx.com/listings/?apikey=472433504f385875&limit=50';
$jsondata = file_get_contents($url);
$json = json_decode($jsondata,true);
$ar_amen_data = array();
foreach ($json['LISTINGS'] as $title_list) {
        $amen = $title_list['AMENITIES'];
        $ar_am = explode(",", $amen);
        foreach ($ar_am as $ar_am_val) {
          if (!empty($ar_am_val)) {
            $ar_amen_data[] = $ar_am_val;
          }
        }
                                           }
    $ar_amen_data = array_count_values($ar_amen_data);

       foreach ($ar_amen_data as $key => $val) {
        if (!empty ($key)){
          global $wpdb;
          $key = esc_sql($key);
          $slag = str_replace(' ', '-', $key);
          $slag_p = esc_sql($slag);

          $taxononi_f = esc_sql("property-feature");
          $table_name = $wpdb->get_blog_prefix() . 'terms';
                  $wpdb->insert(
                    $table_name, array(
                      'name' => $key,
                      'slug' => $slag_p,
                    ), array('%s', '%s'));
          $table_name = $wpdb->get_blog_prefix() . 'terms';
          $lastid = $wpdb->insert_id;
          $table_name = $wpdb->get_blog_prefix() . 'term_taxonomy';
                  $wpdb->insert(
                          $table_name, array(
                      'term_id' => $lastid,
                      'taxonomy' => $taxononi_f,
                      ), array('%d', '%s'));
                          }
                                }
                                          }
function get_property_type(){
$url = 'http://dataapi.realtymx.com/listings/?apikey=472433504f385875&limit=50';
$jsondata = file_get_contents($url);
$json = json_decode($jsondata,true);
$i = "320";
foreach ($json['LISTINGS'] as $title_list) {
        $amen = $title_list['PROPERTY_TYPE'];
        $data_st = "--" . $amen;
        $ar_am = explode("--", $data_st);
                                           }
            foreach ($ar_am as $key) {
              if (!empty ($key)){
                $in = $i++;
                global $wpdb;
                $amenist = esc_sql($key);
                $nam_am = esc_sql($in);
                $slag = str_replace(' ', '-', $amenist);
                $slag_p = esc_sql($slag);
                $taxononi_f = esc_sql("property-type");
                $table_name = $wpdb->get_blog_prefix() . 'terms';
                        $wpdb->insert(
                                $table_name, array(
                            'term_id' => $nam_am,
                            'name' => $amenist,
                            'slug' => $slag_p,
                            ), array('%d', '%s', '%s'));
                $table_name = $wpdb->get_blog_prefix() . 'term_taxonomy';
                        $wpdb->insert(
                                $table_name, array(
                            'term_id' => $nam_am,
                            'taxonomy' => $taxononi_f,
                            ), array('%d', '%s'));
                                }
                                      }
                            }

function get_property_city(){
$url = 'http://dataapi.realtymx.com/listings/?apikey=472433504f385875&limit=50';
$jsondata = file_get_contents($url);
$json = json_decode($jsondata,true);
$i = 400;
$stat1 = [];
$citi1 = [];
foreach ($json['LISTINGS'] as $title_list) {
        $stat = $title_list['STATE'];
        $city = $title_list['CITY'];
        if ($stat == "NY") $stat = "New York";
        if ($city == "BROOKLYNBrooklyn" || "BROOKLYN") $city = "Brooklyn";
        if ($city == "RIDGEWOOD") $city = "Ridgewood";
       $stat1[] = $stat;
       $citi1[] = $city;
                                        }
       $citi1 =  array_count_values($citi1);
       $citi1 = array_flip($citi1);
       $stat1 =  array_count_values($stat1);
       $stat1 = array_flip($stat1);
    $arr_st = [];
    $result = array_merge($arr_st, $citi1);
       foreach ($result as $key) {
        if (!empty ($key)){
          $in = $i++;
          global $wpdb;
          $amenist = esc_sql($key);
          $nam_am = esc_sql($in);
          $slag = str_replace(' ', '-', $amenist);
          $slag_p = esc_sql($slag);
          $taxononi_f = esc_sql("property-city");
          $table_name = $wpdb->get_blog_prefix() . 'terms';
                  $wpdb->insert(
                          $table_name, array(
                      'term_id' => $nam_am,
                      'name' => $amenist,
                      'slug' => $slag_p,
                      ), array('%d', '%s', '%s'));
          $table_name = $wpdb->get_blog_prefix() . 'term_taxonomy';
                  $wpdb->insert(
                          $table_name, array(
                      'term_id' => $nam_am,
                      'taxonomy' => $taxononi_f,
                      ), array('%d', '%s'));
                          }
                                }
                                $result = array_merge($arr_st, $stat1);
                                   foreach ($result as $key) {
                                    if (!empty ($key)){
                                      $in = $i++;
                                      global $wpdb;
                                      $amenist = esc_sql($key);
                                      $nam_am = esc_sql($in);
                                      $slag = str_replace(' ', '-', $amenist);
                                      $slag_p = esc_sql($slag);
                                      $taxononi_f = esc_sql("property-city");
                                      $table_name = $wpdb->get_blog_prefix() . 'terms';
                                              $wpdb->insert(
                                                      $table_name, array(
                                                  'term_id' => $nam_am,
                                                  'name' => $amenist,
                                                  'slug' => $slag_p,
                                                  ), array('%d', '%s', '%s'));
                                      $table_name = $wpdb->get_blog_prefix() . 'term_taxonomy';
                                              $wpdb->insert(
                                                      $table_name, array(
                                                  'term_id' => $nam_am,
                                                  'taxonomy' => $taxononi_f,
                                                  ), array('%d', '%s'));
                                                      }
                                                            }
                                          }
                                          function get_property_status(){
                                          $url = 'http://dataapi.realtymx.com/listings/?apikey=472433504f385875&limit=50';
                                          $jsondata = file_get_contents($url);
                                          $json = json_decode($jsondata,true);
                                          $i = "300";
                                          foreach ($json['LISTINGS'] as $title_list) {
                                                  $amen = $title_list['STATUS'];
                                                  $data_st = "--" . $amen;
                                                  $ar_am = explode("--", $data_st);
                                                                                     }
                                                      foreach ($ar_am as $key) {
                                                        if (!empty ($key)){
                                                          $in = $i++;
                                                          global $wpdb;
                                                          $amenist = esc_sql($key);
                                                          $nam_am = esc_sql($in);
                                                          $slag = str_replace(' ', '-', $amenist);
                                                          $slag_p = esc_sql($slag);
                                                          $taxononi_f = esc_sql("property-status");
                                                          $table_name = $wpdb->get_blog_prefix() . 'terms';
                                                                  $wpdb->insert(
                                                                          $table_name, array(
                                                                      'term_id' => $nam_am,
                                                                      'name' => $amenist,
                                                                      'slug' => $slag_p,
                                                                      ), array('%d', '%s', '%s'));
                                                          $table_name = $wpdb->get_blog_prefix() . 'term_taxonomy';
                                                                  $wpdb->insert(
                                                                          $table_name, array(
                                                                      'term_id' => $nam_am,
                                                                      'taxonomy' => $taxononi_f,
                                                                      ), array('%d', '%s'));
                                                                          }
                                                                                }
                                                                      }


?>
